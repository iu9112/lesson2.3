const express = require("express");
const bodyParser = require("body-parser");
const PORT = 3000;
const app = express();

function isEmptyObject(emptyObject){//функция проверки на пустоту объекта
    return JSON.stringify(emptyObject) === '{}';
}

app.get('/', function (req, res) { // задание 1
	res.status(200).send('Hello, Express.js');
});

app.get('/hello/', function (req, res) { // задание 2
	res.status(200).send('Hello stranger!');
});

app.get('/hello/:name', function (req, res) { // задание 3
	res.status(200).send(`Hello, ${req.params.name}!`);
});

app.all('/sub/*', function (req, res) { // задание 4
	res.status(200).send(`You requested URI: ${req.protocol}://${req.hostname}:${PORT}${req.originalUrl}`);//вывод URI, например http://127.0.0.1:3000/sub/some/second/
	console.log(req.hostname);
});

var urlencodedParser = bodyParser.urlencoded({ extended: false });

const middleware = function (req, res, next) { //middleware - дополнительное задание.
	if(!req.headers.my_header)
	{
		return res.sendStatus(401);
	}
	next();
}

app.post('/post', urlencodedParser,middleware, function (req, res) { //задание 5
  if (isEmptyObject(req.body))
  {
	return res.sendStatus(404);
  }
  else{
	res.send(req.body)
  }
});
app.listen(PORT, () => {
  console.log('Hello! We are live on ' + PORT);
});